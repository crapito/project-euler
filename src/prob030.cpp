#include <iostream>
#include <utility>
#include <vector>
#include <string>
#include <math.h>
#include <algorithm>
#include <execution>
#include <ranges>
#include <numeric>

#include <assert.h>

namespace rv = std::ranges::views;
namespace rg = std::ranges;

#ifdef NDEBUG
#define DEBUG(x) do {} while(0)
#else
#define DEBUG(x) do\
    { std::cout << __FILE__ << ":" << __LINE__ << "\t" << x << '\n';\
    } while(0)
#endif


int main([[maybe_unused]] const int argc, [[maybe_unused]] const char** argv)
{
    const int power = (argc == 2) ? atoi(argv[1]) : 5;

    /* Compute how many digit we need at most */
    const double logVal = static_cast<double>(power) * std::log10(9);
    auto threshold = [&logVal](auto x){return std::ceil(std::log10(x) + logVal) - x;};
    auto positive = [](auto x){return x>=0;};
    auto moreDigitNeeded = [&](auto x){return positive(threshold(x));};
    auto r = rv::iota(1) | rv::drop_while(moreDigitNeeded);
    auto digitNeeded = r.front();
    int maxDigit = std::pow(10, digitNeeded) - 1;

    auto sumPowerDigits = [](auto x, int power)
    {
        auto sum = 0;
        while (x != 0)
        {
            sum = sum + std::pow(x % 10, power);
            x = x / 10;
        }
        return sum;
    };
    auto sumEqualDigit = [&power, &sumPowerDigits](auto x)
    { return (sumPowerDigits(x, power) == x); };
    auto acceptedDigit = rv::iota(2,maxDigit) | rv::filter(sumEqualDigit);

    std::vector<int> finalNumbers;
    for (auto d : acceptedDigit)
        finalNumbers.emplace_back(d);

    auto sum = std::accumulate(finalNumbers.begin(), finalNumbers.end(), 0);


    std::cout << "Numbers:\n";
    for (auto d : finalNumbers)
        std::cout << d << '\n';
    std::cout << "Total sum: " << sum << '\n';



    return 0;
}
