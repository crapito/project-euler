main :: IO ()
main = print $ fst . head $ filter ((>=1000) . length . show . snd) indexedFibs

fibs :: [Integer]
fibs =  1 : 1 : zipWith (+) fibs (tail fibs)

indexedFibs :: [(Int, Integer)]
indexedFibs = zip [1..] fibs
