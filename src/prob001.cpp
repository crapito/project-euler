#include <iostream>
#include <sstream>

#include <string>
#include <vector>

#include <algorithm>
#include <utility>
#include <math.h>
#include <ranges>
#include <numeric>

#include <assert.h>

namespace rv = std::ranges::views;

#ifdef NDEBUG
#define DEBUG(x) do {} while(0)
#else
#define DEBUG(x) do\
    { std::cout << __FILE__ << ":" << __LINE__ << "\t" << x << '\n';\
    } while(0)
#endif

#define DEFAULT_VALUE 1000        // Default value return by the getArg function
size_t getArg(int argc, char** argv);


template <size_t incr = 1>
struct custom_iota_iterator
{
    
    typedef std::ptrdiff_t difference_type; 
    typedef size_t value_type;
    custom_iota_iterator& operator++()
    {
        cur_ += incr;
        return *this;
    }
    const size_t& operator*() const { return cur_; }
    void operator++(int)
    {
        ++*this;
    }
private:
    size_t cur_{0};
};

template <size_t N = 1>
using incr_view = std::ranges::subrange<custom_iota_iterator<N>, std::unreachable_sentinel_t>;

            /* MAIN BODY */
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{

    auto n = getArg(argc, argv);

    auto three = incr_view<3>{} | rv::take_while([&](auto x){ return x < n;});
    auto five  = incr_view<5>{} | rv::take_while([&](auto x){ return x < n;});
    std::vector<int> results;
    std::ranges::set_union(three, five, std::back_inserter(results));

    for (auto x : results)
        DEBUG(x);

    std::cout << "Sum: " << std::accumulate(results.begin(), results.end(), 0zu) << '\n';

    return 0;
}






/* FUNCTION IMLEMENTATION FOR THE TEMPLATE FILE */
size_t getArg(int argc, char** argv)
{
    if (argc < 2)
        return DEFAULT_VALUE;

    std::string str(argv[1]);
    std::stringstream ss(str);
    size_t value;
    ss >> value;
    return value;
}
