import Data.Express.Utils.List (isPermutationOf)
import Data.List (sort)


solution = head $ dropWhile (not . ourCondition) [1..]

ourCondition :: Int -> Bool
ourCondition x = allSame $ map (sort . show . (*x)) [2..6]

allSame :: Eq a => [a] -> Bool
allSame = and . (zipWith (==) <*> tail)
