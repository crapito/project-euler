#include <iostream>
#include <utility>
#include <vector>
#include <algorithm>
#include <string>
#include <math.h>

//#define NDEBUG Define to ignore assertion, stands for NoDEBUG
#include <assert.h>

#include "../include/BigInt.hpp"

// Compute the sum of the digit of n!
int main(const int argc, const char** argv)
{
    if (argc < 2)
    {
        std::cerr << "Usage: prob20 <n>\n";
        return 1;
    }

    int n = atoi(argv[1]);

    BigInt fac(1);

    for (int i = 2; i <= n; ++i)
    {
        fac *= i;
    }

    int sumDigits = fac.sumDigits();
    std::cout << sumDigits << '\n';


    return 0;
}