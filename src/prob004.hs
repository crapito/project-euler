
main :: IO()
main = print $ largestPalindromicSumUsing 3

isPalindromic :: Integer -> Bool
isPalindromic x = x == ( read . reverse . show $ x ) 

largestPalSum :: Integer -> Integer
largestPalSum x = maximum [i*j | i <- [1..x], j <- [1..x], i <= j, isPalindromic (i*j)]

largestPalindromicSumUsing :: Integer -> Integer
largestPalindromicSumUsing x = largestPalSum $ (^) 10 x