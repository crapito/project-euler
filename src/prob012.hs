import Data.List
main :: IO()
main = print $ head $ filter ((>500) . numDivisors) triangulars

triangulars :: [Int]
triangulars = map (\x -> x * (x+1) `div` 2) [1..]

isSquare :: (Integral a) => a -> Bool
isSquare n = mySqrtRound n ^ 2 == n

mySqrtRound :: (Integral a) => a -> a
mySqrtRound = round . sqrt . fromIntegral

numDivisors :: Int -> Int
numDivisors x
    | isSquare x = 1 + 2 * length [a | a <- [1.. (mySqrtRound x - 1)], x `mod` a == 0]
    | otherwise = 2 * length [a | a <- [1.. mySqrtRound x], x `mod` a == 0]
