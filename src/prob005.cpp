#include <iostream>
#include <sstream>

#include <string>
#include <vector>

#include <algorithm>
#include <functional>
#include <utility>
#include <cmath>
#include <ranges>
#include <numeric>

#include <cassert>

namespace rv = std::ranges::views;

#ifdef NDEBUG
#define DEBUG(x) do {} while(0)
#else
#define DEBUG(x) do\
    { std::cout << __FILE__ << ":" << __LINE__ << "\t" << x << '\n';\
    } while(0)
#endif

#define DEFAULT_VALUE 20        // Default value return by the getArg function
size_t getArg(int argc, char** argv);

            /* MAIN BODY */
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{

    auto upper = getArg(argc, argv);

    auto reduceFactors = [](size_t x, const auto& vec){
        for (const auto& d : vec)
            if (x % d == 0)
                x /= d;
        return x;
    };

    auto push_while = [](size_t x, size_t k, auto& vec){
        while (x>1 && x % k == 0)
        {
            vec.push_back(k);
            x /= k;
        }
        return x;
    };

    auto appendFactors = [push_while](size_t x, auto& vec){
        x = push_while(x, 2, vec);
        for (size_t k = 3; k <= x; k += 2)
            x = push_while(x, k, vec);
    };

    std::vector<size_t> div;
    for (auto i : rv::iota(2zu, upper))
    {
        i = reduceFactors(i, div);
        appendFactors(i, div);
    }

    auto res = std::accumulate(begin(div), end(div), 1zu, std::multiplies<>{});

    std::cout << res << '\n';
    return 0;
}






/* FUNCTION IMPLEMENTATION FOR THE TEMPLATE FILE */
size_t getArg(int argc, char** argv)
{
    if (argc < 2)
        return DEFAULT_VALUE;

    std::string str(argv[1]);
    std::stringstream ss(str);
    size_t value;
    ss >> value;
    return value;
}
