#include <iostream>
#include <utility>
#include <vector>
#include <algorithm>
#include <string>
#include <math.h>

//#define NDEBUG Define to ignore assertion, stands for NoDEBUG
#define DEBUG
#include <assert.h>

// Polynome
int P(const int& n, const int& a, const int& b)
{
    return n*n + a*n + b;
}

bool isPrime(const int& k)
{
    if (k < 2) return false;
    if ( k == 2 ) return true;
    if ( !(k % 2)) return false;

    static std::vector<int> primes = {2};

    // Check if we have it in our vector
    auto find_it = std::find(primes.begin(), primes.end(), k);
    if ( find_it != primes.end() ) return true;
    if ( primes.end() == find_it && k < primes.back() ) return false;

    // If cannot be determined, we have to use a sieve to check

    std::vector<int> sieve((k-1)/2, 1);

    for (int p = 0; p < ( primes.back() - 1 ) / 2; p++)
        sieve[p] = 0; 

    for (auto p : primes)
    {
        if ( p == 2 ) continue;
        sieve[(p-1)/2 - 1] = 1;
    }

    size_t i = 3;

    while (primes.back() < k && i <= static_cast<size_t>(k))
    {
        // If not already crossed, means it is prime
        if ( sieve[ (i-1)/2 - 1 ] )
        {
            if ( primes.back() < static_cast<int>(i) )
            { primes.push_back( static_cast<int>(i) ); }

            for (size_t j = i; (j-1) / 2 <= sieve.size(); j += 2*i)
            {
                sieve[(j-1)/2 - 1] = 0;
            }
        }
        i+=2;

    }

    if (primes.back() == k) return true;
    else return false;

}

int main(const int argc, const char** argv)
{
    int n = 1000;
    if (argc > 2)
    {
        std::cerr << "Usage: prob027 <max of |a| & of |b|>\n";
        return 1;
    }
    else if (argc == 2)
    {
        n = atoi(argv[1]);
    }


    int a_max = 0, b_max = 0, n_max = 0;
    for (int a = -n+1; a < n; a++)
    {
        for (int b = 2; b <= n; b++)
        {

            int count = 0;
            while (isPrime( P(count++, a, b) ))
            { }
            if (--count > n_max)
            {
                a_max = a;
                b_max = b;
                n_max = count;
            }

        }
    }

    std::cout << "(a,b): (" << a_max << ", " << b_max
                    << ") -- n: " << n_max << '\n';

    #ifdef DEBUG
    std::cout << "Prime list generated:\n";

    for (int i = 0; i < n_max; i++)
        std::cout << P(i, a_max, b_max) << ' ';

    std::cout << '\n';
    #endif

    return 0;
}