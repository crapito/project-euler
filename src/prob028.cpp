#include <iostream>
#include <utility>
#include <vector>
#include <algorithm>
#include <string>
#include <math.h>

//#define NDEBUG Define to ignore assertion, stands for NoDEBUG
#include <assert.h>

int main(const int argc, const char** argv)
{
    if (argc < 2)
    {
        std::cerr << "Usage: prob028 <n>\n";
        return 1;
    }

    int n = atoi(argv[1]);

    int sum = 1, i = 1, it = 0;
    while ( it + 1 < n)
    {
        it += 2;
        for (int k = 0; k < 4; k++)
        {
            i += it;
            //std::cout << i << ' ';
            sum += i;
        }

    }
    std::cout << sum << '\n';


    return 0;
}