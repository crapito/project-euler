#include <iostream>
#include <utility>
#include <vector>
#include <algorithm>
#include <string>
#include <math.h>


int main(const int argc, const char** argv)
{
    if (argc < 2)
    {
        std::cerr << "Usage: prob016 <powerOf2>\n";
        return 1;
    }

    int n = atoi(argv[1]);

    int nDigits = ceil( log10(2) * n );

    std::vector<uint> num(nDigits, 0);
    num[nDigits - 1] = 1;

    for (int i = 0; i < n; i++)
    {
        uint carry = 0;

        for (auto Dit = num.rbegin(); Dit != num.rend(); Dit++)
        {
            uint mult = 2 * (*Dit) + carry;
            *Dit = (mult % 10);
            carry = (mult / 10);
        }
    }

    int sum = 0;
    std::cout << "2^" << n << " is: ";
    for (auto k : num)
    {
        std::cout << k;
        sum += k;
    }
    std::cout << "\nAnd the sum of the digits is: " << sum << '\n';




    return 0;
}