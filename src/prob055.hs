module Main where

main :: IO()
main = print $ length $ filter isLychrel [1..10000]

isLychrel :: Integer -> Bool
isLychrel x = isLychrel' x 0

isLychrel' :: Integer -> Int -> Bool
isLychrel' x acc
  | acc > 50 = True
  | isPalindromic lx = False
  | otherwise = isLychrel' lx (acc + 1)
  where lx = lychrelTransform x

maxIter :: Int
maxIter = 50

lychrelTransform :: Integer -> Integer
lychrelTransform x = x + read (reverse $ show x)

isPalindromic :: Integer -> Bool
isPalindromic x = s == reverse s
        where s = show x
