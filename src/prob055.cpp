#include <iostream>
#include <utility>
#include <vector>
#include <string>
#include <math.h>
#include <algorithm>
#include <functional>
#include <ranges>
#include <sstream>
#include <numeric>

#include <assert.h>

namespace rv = std::ranges::views;

#ifdef NDEBUG
#define DEBUG(x) do {} while(0)
#else
#define DEBUG(x) do\
    { std::cout << __FILE__ << ":" << __LINE__ << "\t" << x << '\n';\
    } while(0)
#endif

size_t reverse(size_t num)
{
    std::string str = std::to_string(num);
    std::reverse(str.begin(), str.end());
    std::stringstream ss(str);
    size_t res;
    ss >> res;
    return res;
}

bool isPalindrome(size_t num)
{
    std::string str = std::to_string(num);
    auto forward = str | rv::take(str.size() / 2 );
    auto backward = str | rv::reverse | rv::take(str.size() / 2 );
    return std::ranges::equal(forward, backward);
}

bool isLychrel(size_t num, size_t iter = 0)
{
    auto sum = num + reverse(num);
    if ( isPalindrome(sum) )
        return false;
    else if (iter >= 50)
        return true;
    else
        return isLychrel(sum, ++iter);
}

int main([[maybe_unused]] const int argc, [[maybe_unused]] const char** argv)
{
    constexpr size_t upperLimit = 10'000;


    size_t count{0};
    for ( auto r : rv::iota(1zu, upperLimit) )
        if (isLychrel(r, 0)) ++count;
    std::cout << count << '\n';

    return 0;
}
