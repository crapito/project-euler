
n :: Integer
n = 100

main :: IO()
main =  print $ sum [1..n]^2 - sum (map (^2) [1..n])