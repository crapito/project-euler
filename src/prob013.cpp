#include <iostream>
#include <utility>
#include <vector>
#include <algorithm>
#include <string>
#include <math.h>


std::string manualSum( const std::vector<std::string>& num);

int main(const int argc, const char** argv)
{
    if (argc < 2)
    {
        std::cerr << "Usage: prob013 <n_1> <n_2> ... <n_k>\n";
        return 1;
    }

    std::vector<std::string> numbers;
    numbers.reserve(argc - 1);

    for (int i = 1; i < argc; i++)
    {
        numbers.emplace_back( argv[i] );
    }

    std::string res = manualSum(numbers);
    std::cout << "\n\n\nSum is equal to: " << res <<
        "\nAnd the first 10 numbers are: " << res.substr(0, 10) << '\n';


    return 0;
}

void reverseSTR(std::string& str)
{
    str = std::string(str.rbegin(), str.rend());
}

std::string manualSum( const std::vector<std::string>& num)
{
    std::string result = "";
    int carry = 0;

    auto itToMax = std::max_element(num.begin(), num.end(),
    [](const std::string& a, const std::string& b) -> bool
    {
        return a.size() < b.size();
    });

    size_t NDigits = (*itToMax).size();
    

    for (size_t digit = 1; digit <= NDigits; digit++)
    {
        int sum = carry;
        for (const auto& w : num)
        {
            auto it = w.end();
            std::advance( it, -digit );
            sum += *it - '0';
        }

        result.append(std::to_string(sum % 10));
        carry = sum / 10;
    }
    result.append( std::to_string(carry) );

    reverseSTR(result);
    return result;
}
