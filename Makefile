CXXVERSION := c++2b
CXXFLAGS = -g -Wall -Wextra -pedantic -O2 -std=$(CXXVERSION)
LDFLAGS = -ltbb

# Get all sources
SRCDIR := src
SRC := $(wildcard $(SRCDIR)/prob*.cpp)

# Defining the basenames of the source files
BINNAMES := $(SRC:$(SRCDIR)/%.cpp=%)

# Target binaries
BINDIR := build
BIN := $(BINNAMES:%=$(BINDIR)/%)

# Defining necessary things for objects
OBJDIR := .objs
OBJS := $(BINNAMES:%=$(OBJDIR)/%.o)

# Defining dependency related stuff
DEPDIR := .deps
DEPFLAGS = -MT $@ -MMD -MP -MF $(DEPDIR)/$*.d 

#################################################
#####             Actual Rules              #####
#################################################

all: $(BIN)

# Linking of the object files
$(BINDIR)/%: $(OBJDIR)/%.o $(DEPFILES) | $(BINDIR)
	$(CXX) $(CXXFLAGS) -o $@ $< $(LDFLAGS)

#Each exec in build dir depends on its src file, build them correctly
$(OBJDIR)/%.o: $(SRCDIR)/%.cpp $(DEPDIR)/%.d | $(OBJDIR) $(DEPDIR)
	$(CXX) $(DEPFLAGS) $(CXXFLAGS) -c -o $@ $< 

# Create dir if not presents
$(BINDIR): ; @mkdir -p $@
$(DEPDIR): ; @mkdir -p $@
$(OBJDIR): ; @mkdir -p $@


clean:
	@rm -rfv $(BINDIR)
	@rm -rfv $(OBJDIR)
	@rm -rfv $(DEPDIR)
	@rm -fv gmon.out

release: release_build
release_build: CXXFLAGS = -O3 -flto -std=$(CXXVERSION) -g -march=native -DNDEBUG
release_build: all

# Use to define the dependencies
DEPFILES := $(BINNAMES:%=$(DEPDIR)/%.d)
$(DEPFILES):
include $(wildcard $(DEPFILES))
