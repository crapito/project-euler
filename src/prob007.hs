
main :: IO()
main = print $ primes !! 10_000

factor :: Integer -> Integer -> [Integer]
factor _ 1 = []
factor d n
    | d * d > n = [n]
    | n `mod` d == 0 = d : factor d (n `div` d)
    | otherwise = factor (d+1) n

primeFactors :: Integer -> [Integer]
primeFactors = factor 2


primes = 2 : filter (null . tail . primeFactors) [3,5..]