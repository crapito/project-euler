#include <iostream>
#include <sstream>

#include <string>
#include <vector>

#include <algorithm>
#include <utility>
#include <math.h>
#include <ranges>
#include <numeric>

#include <assert.h>

namespace rv = std::ranges::views;

#ifdef NDEBUG
#define DEBUG(x) do {} while(0)
#else
#define DEBUG(x) do\
    { std::cout << __FILE__ << ":" << __LINE__ << "\t" << x << '\n';\
    } while(0)
#endif

#define DEFAULT_VALUE 1000zu
size_t getArg(int argc, char** argv)
{
    if (argc < 2)
        return DEFAULT_VALUE;

    std::string str(argv[1]);
    std::stringstream ss(str);
    size_t value;
    ss >> value;
    return value;
}

int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{

    const auto n = getArg(argc, argv);
    const size_t upperLimit = std::ceil(std::sqrt(n) / std::sqrt(2.0)) + 1zu;

    const auto isPalindromic = [](auto num)
    {
        std::string str(std::to_string(num));
        auto forward = str | rv::take(str.size() / 2);
        auto backward = str | rv::reverse | rv::take(str.size() / 2);
        return std::ranges::equal(forward, backward);
    };
    const auto squared = [](auto x){return x*x;};

    /* Loop and add every palindromic sum of consecutive numbers */
    std::vector<size_t> results;
    for (auto i : rv::iota(1zu,upperLimit))
    {
        size_t sum{i*i};
        for (auto next : rv::iota(i+1zu, upperLimit) | rv::transform(squared))
        {
            sum += next;
            if (sum > n)
                break;
            if (isPalindromic(sum))
                results.push_back(sum);
        }
    }

    /* Delete the duplicates */
    std::sort(results.begin(), results.end());
    auto toErase = std::unique(results.begin(), results.end());
    results.erase(toErase, results.end());

    for (auto x : results)
        DEBUG(x);

    auto sum = std::accumulate(results.begin(), results.end(), 0zu);
    std::cout << "Sum:\t" << sum << '\n';
}
