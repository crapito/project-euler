#include <iostream>
#include <utility>
#include <vector>
#include <algorithm>
#include <string>
#include <math.h>
#include <numeric>

// Returns the n-th prime
long unsigned int sumPrimesTo(const int &n)
{
    if ( n < 0)
    {
        std::cerr << "n has to be positive!\n";
        exit(EXIT_FAILURE);
    }
    else if ( n < 2) return 0;


    std::vector<int> crible( (n/2)+1, 1);
    std::vector<int> primes = {2};

    for (size_t i = 3; i <= (size_t)n; i += 2)
    {

        if (crible[(i-1)/2])
        {
            primes.push_back(i);
            for (size_t k = i; k < crible.size()*2 + 1; k += 2*i)
            {
                crible[(k-1)/2] = 0;
            }

        }
    }

    return std::accumulate(primes.begin(), primes.end(), (long unsigned int)0);

}

int main(const int argc, const char** argv)
{
    if (argc < 2)
    {
        std::cerr << "Usage: prob010 <upper_bound>\n";
        return 1;
    }

    int n = atoi(argv[1]);

    std::cout << "Sum of primes up to " << n << " is: " << sumPrimesTo(n) << '\n';

    return 0;
}