#include <iostream>
#include <sstream>

#include <string>
#include <vector>
#include <set>

#include <algorithm>
#include <utility>
#include <math.h>
#include <ranges>
#include <numeric>

#include <assert.h>

namespace rv = std::ranges::views;

#ifdef NDEBUG
#define DEBUG(x) do {} while(0)
#else
#define DEBUG(x) do\
    { std::cout << __FILE__ << ":" << __LINE__ << "\t" << x << '\n';\
    } while(0)
#endif

#define DEFAULT_VALUE 600'851'475'143     // Default value return by the getArg function
size_t getArg(int argc, char** argv);


            /* MAIN BODY */
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{

    auto n = getArg(argc, argv);

    std::set<size_t> factors;

    while (n > 1 && n % 2 == 0)
    {
        factors.insert(2);
        n /= 2;
    }

    for (size_t k = 3; k <= n; k += 2)
    {
        while (n>1 && n % k == 0)
        {
            factors.insert(k);
            n /= k;
        }
    }

    std::cout << *factors.rbegin() << '\n';
    return 0;
}






/* FUNCTION IMPLEMENTATION FOR THE TEMPLATE FILE */
size_t getArg(int argc, char** argv)
{
    if (argc < 2)
        return DEFAULT_VALUE;

    std::string str(argv[1]);
    std::stringstream ss(str);
    size_t value;
    ss >> value;
    return value;
}
