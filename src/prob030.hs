import Data.Char

main :: IO ()
main = print $ sum solution

solution :: [Int]
solution = [ a | a <- [2..upperLimit], a == sumDigitWith (\x -> x*x*x*x*x) a]

sumDigitWith :: (Int -> Int) -> Int -> Int
sumDigitWith f x = sum $ map (f . digitToInt) $ show x

upperLimit :: Int
upperLimit = 400000
