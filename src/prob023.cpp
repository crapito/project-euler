#include <iostream>
#include <utility>
#include <vector>
#include <algorithm>
#include <map>
#include <string>
#include <math.h>

//#define NDEBUG Define to ignore assertion, stands for NoDEBUG
#include <assert.h>

/*
I know this is very inefficient, we should compute possible sums of
two abundent numbers s.t. n + m < 28123 and substract it to the sum
of the first 28123 numbers.
*/

void GetDiv(const int& k, std::vector<int>& v)
{
    v.clear();
    for (int i = 1; i < k / 2; i++)
    {
        if (v.size() > 0)
        {
            if (v.back() <= i)
            return ;
        }
        if (!(k%i))
        {
            v.push_back(i);
            v.push_back(k / i);
        }
    }

    return ;
}

// Returns 0 if not abundant, returns the sum of its proper divisor if abundant
int isAbundant(const int& k)
{
    // Will store results of abunduncy
    static std::map<int, int> abun;
    if (abun.count(k) > 0) return abun[k];


    std::vector<int> vec;
    GetDiv(k, vec);

    // Delete duplicates
    std::sort(vec.begin(), vec.end());
    auto it = std::unique(vec.begin(), vec.end());
    // As we want proper divisors, we have to exclude the number itself
    vec.resize(std::distance(vec.begin(), it - 1));

    // Compute the sum
    int sum = 0;
    for (auto v : vec)
        sum += v;

    abun[k] = (k < sum) * sum;
    return abun[k];
}

int main(int , char** )
{

    int n = 28123;

    int sum = 0;
    // Check all number up to n if they can be sum of two abund
    for (int k = 1; k <= n; k++)
    {
        // Loop through possible sum and check if they are abundants
        for (int i = 12; i <= k / 2; i++)
        {
            if ( isAbundant(k - i) <= 0 ) continue;
            if ( isAbundant(i) > 0)
            {
                sum += k;
                break;
            }

        }

    }


    std::cout << n*(n+1) / 2 - sum << '\n';

    return 0;
}
