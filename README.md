# Project Euler
Project euler is a collection of problem set you have to solve using your favorite programming language.
This git will contain my solution written mostly in C++, and perhaps sometimes in python.

Link: [Project Euler Archives](https://projecteuler.net/archives)

### Note
I do not claim my solutions are the most elegants nor the most efficients.
I will use these problem to learn more about whatever language used.
Hence solutions might be over-engineered and rarely optimal.  
E.g., my implementation of arbitrarly large integer arithmetic (see `include/BigInt.hpp`) is clearly slow and (at the time beeing) I don't really intend to make it faster.
I will often prefer to use STL algorithms and particularly ranges which I really want to learn better.

- - - -

## Requirement
### C++
I only implemented a simple build process using a single Makefile.
You need to have a Make tool installed and a C++ compiler supporting the C++20 standards---at this time, I use the `c++2b` flag with `gcc`.

Once cloned, you just need to execute `make` to build all the solutions---`make release` may be more suitable.
Then you can run them by going into the build directory or by running them directly, e.g.

    $ ./build/prob010 100
    > Sum of primes up to 100 is: 1060

### Python
You must be able to run Python3 code on your machine and have some standards python libraries installed.  
When Python is used, you won't find the executable in the build folder.
Instead just run the source file directly with python, e.g. for the problem 22:

        $ python3 src/prob022.py

### Remarks
- The first time you build the project can be pretty slow, you can use `make -j<n>` to use multiple core and speed up the build stage (where `<n>` is replaced by the number of cores you want to utilise).
- The first compilation will generate dependencies files (stored in `.deps`), meaning the second `make` will also recompile all the files.
Then from the third on (unless `make clean`), only the modified files will recompile.
- Default make target is debug and will not be optimized during compilation.
You can use the `release` target to get better performing executables:

        $ make clean && make release -j4


- - - -

### TODO

### List
Just a big list of the problem I solved with the language(s) I used.

001-016: C++ and Haskell  
017: C++  
020: C++  
022: Python  
023: C++  
025: C++  
027: C++  
028: C++  
030: C++  
042: Python  
045: C++  
048: C++  
055: C++  
125: C++  
145: C++  
