import Data.List (tails, nub)


factor' :: Int -> Int -> [Int]
factor' _ 1 = []
factor' d n
    | d*d > n = [n]
    | n `mod` d == 0 = d : factor' d (n `div` d)
    | otherwise = factor' (d+1) n

factor :: Int -> [Int]
factor = factor' 2

consec :: Int -> [[Int]]
consec n = foldr (zipWith (:)) (repeat []) (take n $ tails [2..])

truthList :: Int -> [Bool]
truthList n = map (all ((==n) . length . nub . factor)) (consec n)

solution :: Int
solution = head . snd $ head $ dropWhile (not . fst) (zip (truthList 4) (consec 4))
