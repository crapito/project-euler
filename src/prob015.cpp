#include <iostream>
#include <utility>
#include <vector>
#include <algorithm>
#include <cstdint>
#include <string>
#include <math.h>

typedef uint64_t luint;

luint choose(const luint& n, const luint& k)
{
    if ( k == 0) return 1;

    return (n * choose(n - 1, k - 1)) / k;
}

int main(const int argc, const char** argv)
{
    if (argc < 2)
    {
        std::cerr << "Usage: prob015 <gridSize>\n";
        return 1;
    }

    luint n = static_cast<luint>( atoi(argv[1]) );

    // number of possibility if 
    std::cout << choose(2*n, n) << '\n';


    return 0;
}
