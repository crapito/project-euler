#include <iostream>
#include <utility>
#include <vector>
#include <algorithm>
#include <string>
#include <cstdint>
#include <math.h>
#include <map>
uint64_t step(uint64_t k)
{
    if (k % 2) return (3*k) + 1;
    
    return k / 2;
}

uint64_t fullCycle(const uint64_t& k, std::map<uint64_t, uint64_t>& map)
{
    if ( k == 1) return 1;
    if (map.count(k))
    {
        return map[k];
    }

    map[k] = fullCycle(step(k), map) + 1;
    return map[k];
}

int main(const int argc, const char** argv)
{
    if (argc < 2)
    {
        std::cerr << "Usage: prob014 <upper bound>\n";
        return 1;
    }

    uint64_t n = static_cast<uint64_t>( atoi(argv[1]) );

    std::map<uint64_t, uint64_t> map;

    for (uint64_t i = 1; i <= n; i++)
    {
        fullCycle(i, map);
    }
    typedef std::pair<uint64_t, uint64_t> MapElem;

    auto maxIt = std::max_element(map.begin(), map.end(),
                    [](const MapElem& a, const MapElem& b) -> bool
                    {
                        return a.second < b.second;
                    });

    std::cout << maxIt->first << " produce a chain of " <<
                maxIt->second << " elements.\n";


    maxIt = std::max_element(map.begin(), map.end(),
                [](const MapElem& a, const MapElem& b) -> bool
                {
                    return a.first < b.first;
                });
    
    std::cout << "The maximum number reached in the chain is: " << maxIt->first << '\n';


    return 0;
}
