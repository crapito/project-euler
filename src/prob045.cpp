#include <iostream>
#include <utility>
#include <vector>
#include <algorithm>
#include <string>
#include <math.h>

//#define NDEBUG Define to ignore assertion, stands for NoDEBUG
#include <assert.h>

// Returns the n-th triangle number
int triangle(const long int& n)
{
    return n*(n+1) / 2;
}


// Check if number is pentagonal
bool isPenta(const long int& n)
{
    double r = (1.0 + sqrt(24.0 * static_cast<double>(n) + 1.0)) / 6.0;
    return ( floor(r) == r );
}

// Check if number is hexagonal
bool isHexa(const int& n)
{
    double r = (1.0 + sqrt(8.0 * static_cast<double>(n) + 1.0)) / 4.0;
    return ( floor(r) == r );
}

int main(int , char** )
{

    long int increment = 286;

jump:
    while ( !isPenta( triangle(increment) ) )
    { ++increment; }

    if ( !isHexa( triangle(increment) ) )
    { ++increment; goto jump; }

    std::cout << increment << " : " << triangle(increment) << '\n';

    return 0;
}
